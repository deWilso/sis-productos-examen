<?php

namespace App\Http\Controllers;
use App\Producto;
use App\Proveedor;
use App\Precio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProducts()
    {       

        $allProducts = DB::select('select prod.nombre, pro.nombre as proveedor, pre.precio, pre.descripcion from productos prod inner JOIN proveedors pro on prod.proveedor = pro.id INNER JOIN precios pre on prod.id = pre.producto');
        return response()->json($allProducts, 200);
    }

   
}
