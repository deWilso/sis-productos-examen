<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    protected $table = 'productos';
    protected $fillable = [
        'id', 'nombre','descripcion', 'proveedor'
    ];  
    public function nombre_proveedor()
    {
        return $this->belongsTo('App\Proveedor');
    }
}
