<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    
    protected $table = 'proveedors';
    protected $fillable = [
        'id', 'nombre'
    ];  
    public function productos()
    {
        return $this->hasMany('App\Producto');
    }
}
