<?php

use Illuminate\Database\Seeder;

class precioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('precios')->insert([
            'descripcion'=>'Mayoreo',
            'precio'=>'20',
            'producto'=>'4'

        ]);
        DB::table('precios')->insert([
            'descripcion'=>'Unitario',
            'precio'=>'25',
            'producto'=>'3'

        ]);
        DB::table('precios')->insert([
            'descripcion'=>'Ofertas',
            'precio'=>'400',
            'producto'=>'2'

        ]);
        DB::table('precios')->insert([
            'descripcion'=>'Liquidacion',
            'precio'=>'15',
            'producto'=>'4'

        ]);
        DB::table('precios')->insert([
            'descripcion'=>'Unitario',
            'precio'=>'15',
            'producto'=>'5'

        ]);

        
        DB::table('precios')->insert([
            'descripcion'=>'Ofertas',
            'precio'=>'300',
            'producto'=>'2'

        ]);

        DB::table('precios')->insert([
            'descripcion'=>'Unitario',
            'precio'=>'200',
            'producto'=>'6'

        ]);
        DB::table('precios')->insert([
            'descripcion'=>'Unitario',
            'precio'=>'1200',
            'producto'=>'8'

        ]);

        DB::table('precios')->insert([
            'descripcion'=>'Mayoreo',
            'precio'=>'35',
            'producto'=>'3'

        ]);
        


      
    }
}
