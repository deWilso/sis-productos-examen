<?php

use Illuminate\Database\Seeder;

class productoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'nombre'=>'Lavamanos de porcelana',
            'descripcion'=>'lavamos para toda ocasion',
            'proveedor'=>'2'

        ]);

        DB::table('productos')->insert([
            'nombre'=>'Lavamanos de porcelana',
            'descripcion'=>'lavamanos fragil',
            'proveedor'=>'3'

        ]);
        DB::table('productos')->insert([
            'nombre'=>'Leche condensada',
            'descripcion'=>'Apto para galletas',
            'proveedor'=>'1'
        ]);

        DB::table('productos')->insert([
             'nombre'=>'Cornflex',
            'descripcion'=>'Tiene dos perritos en la caja',  
            'proveedor'=>'1'
        ]);
        DB::table('productos')->insert([
            'nombre'=>'jabon ambar',
            'descripcion'=>'jabon  para ropas blancas',
            'proveedor'=>'1'
        ]);
        DB::table('productos')->insert([
            'nombre'=>'vidrios acrilicos',
            'descripcion'=>'apto para pc gamer',
            'proveedor'=>'2'
        ]);
        DB::table('productos')->insert([
            'nombre'=>'Inodoros de metal',
            'descripcion'=>'disfrutelo',
            'proveedor'=>'2'
        ]);
        DB::table('productos')->insert([
            'nombre'=>'Extension tomacorriente',
            'descripcion'=>'para pared',
            'proveedor'=>'3'
        ]);
    }
}
