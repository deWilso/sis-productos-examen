<?php

use Illuminate\Database\Seeder;

class proveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proveedors')->insert([
            'nombre'=>'Tienda la finquita',
        ]);
        DB::table('proveedors')->insert([
            'nombre'=>'Cemaco',
        ]);
        DB::table('proveedors')->insert([
            'nombre'=>'Distribuidora Leal',
        ]);
    }
}
